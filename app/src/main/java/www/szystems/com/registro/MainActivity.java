package www.szystems.com.registro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre, txtApellidos, txtPass1, txtPass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellidos = (EditText) findViewById(R.id.txtApellidos);
        txtPass1 = (EditText) findViewById(R.id.txtPass1);
        txtPass2 = (EditText) findViewById(R.id.txtPass2);
    }

    public void login(View view)
    {
        String nombre = txtNombre.getText().toString();
        String apellidos = txtApellidos.getText().toString();
        String contra1 = txtPass1.getText().toString();
        String contra2 = txtPass2.getText().toString();


        if(!nombre.equals("") || !apellidos.equals("") || !contra1.equals("") || !contra2.equals(""))
        {
            if (contra1.equals(contra2))
            {
                Intent i = new Intent(this, QuestActivity.class);
                startActivity(i);
            }
            else
            {
                Toast notification = Toast.makeText(this, "Las contraseñas no coinciden ", Toast.LENGTH_SHORT);
                notification.show();
            }
        }
        else
        {

            Toast notification = Toast.makeText(this, "Algun campo esta vacio ", Toast.LENGTH_SHORT);
            notification.show();
        }
    }
}
